#ifndef BLINK_H
#define BLINK_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
  class Blink;
}

class Blink : public QMainWindow
{
  Q_OBJECT

public:
  explicit Blink(QWidget *parent = 0);
  ~Blink();

private slots:
  void on_startButton_clicked();

  void on_stopButton_clicked();

  void on_quitButton_clicked();

  void flash_start();

  void flash_stop();

  void timerEvent(QTimerEvent *event);

  void on_flash_rateSlider_sliderMoved(int position);

private:
  Ui::Blink *ui;
  int flash_timer_id;
};

#endif // BLINK_H
