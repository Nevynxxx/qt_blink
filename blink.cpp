#include "blink.h"
#include "ui_blink.h"

Blink::Blink(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::Blink)
{
    ui->setupUi(this);
    ui->lcdNumber->display(ui->flash_rateSlider->value());
}

Blink::~Blink()
{
  delete ui;
}

void Blink::on_startButton_clicked()
{
    flash_start();
    ui->statusBar->showMessage("Running");
}

void Blink::on_stopButton_clicked()
{
  flash_stop();
  ui->statusBar->showMessage("Stopped");
}

void Blink::on_quitButton_clicked()
{
  qApp->quit();
}

void Blink::flash_start()
{
  flash_timer_id = startTimer(ui->flash_rateSlider->value());

}

void Blink::flash_stop()
{
 killTimer(flash_timer_id);
}

void Blink::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == flash_timer_id)
      {
        if (ui->flashSimulator->isChecked())
          ui->flashSimulator->setChecked(false);
        else
          ui->flashSimulator->setChecked(true);
      }
}

void Blink::on_flash_rateSlider_sliderMoved(int position)
{
  int currentRateInt = ui->flash_rateSlider->value();

  killTimer(flash_timer_id);
  flash_timer_id = startTimer(currentRateInt);

  ui->lcdNumber->display(currentRateInt);
}
