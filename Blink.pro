#-------------------------------------------------
#
# Project created by QtCreator 2014-04-22T18:53:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Blink
TEMPLATE = app


SOURCES += main.cpp\
        blink.cpp

HEADERS  += blink.h

FORMS    += blink.ui
